package com.example.atividade06;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    ArrayList<Pessoa> pessoas = new ArrayList<>();
    ArrayList<String> nomes = new ArrayList<>();
    ListView lista;
    EditText cpf, nome, telefone, celular, email, buscar;
    CalendarView nascimento;
    Button add, apaga;
    ArrayAdapter adapter;
    int dia,ano, mes;
    String data = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lista = findViewById(R.id.Lista);
        cpf = findViewById(R.id.Cpf);
        nome = findViewById(R.id.Nome);
        nascimento = findViewById(R.id.Calendario);
        telefone = findViewById(R.id.Telefone);
        celular = findViewById(R.id.Celular);
        email = findViewById(R.id.Email);
        buscar = findViewById(R.id.Busca);
        add = findViewById(R.id.Add);
        apaga = findViewById(R.id.Remove);


        adapter = new ArrayAdapter(MainActivity.this
                , R.layout.layout_lista,nomes );
        lista.setAdapter(adapter);

        nascimento.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
                dia = i2;
                mes = i1+1;
                ano = i;

                data = String.valueOf(dia)+"-"+String.valueOf(mes)+"-"+String.valueOf(ano);
            }
        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

             if((cpf.getText().toString().isEmpty()) || (nome.getText().toString().isEmpty()) ||
                     (celular.getText().toString().isEmpty()) || (telefone.getText().toString().isEmpty()) ||
                     (email.getText().toString().isEmpty())){
                 Toast.makeText(MainActivity.this,"Campos Vazios, Por favor Preencher", Toast.LENGTH_LONG).show();
             }else{

                 Pessoa pessoa = new Pessoa();
                 pessoa.setCpf(cpf.getText().toString());
                 pessoa.setNome(nome.getText().toString());
                 pessoa.setNascimento( data);
                 pessoa.setCelular(celular.getText().toString());
                 pessoa.setTelefone(telefone.getText().toString());
                 pessoa.setEmail(email.getText().toString());
                 pessoas.add(pessoa);
                 nomes.add(pessoa.getNome());
                 adapter.notifyDataSetChanged();
                 lista.setAdapter(adapter);
                 String linha = "Pessoa adicionada : "+pessoa.getNome()+" "+pessoa.getCpf()+" "+pessoa.getNascimento()
                         +" "+pessoa.getTelefone()+" "+pessoa.getCelular()+" "+pessoa.getEmail();
                 Toast.makeText(MainActivity.this, linha, Toast.LENGTH_SHORT).show();
                 cpf.setText("");
                 nome.setText("");
                 celular.setText("");
                 telefone.setText("");
                 email.setText("");
                 nascimento.setDate(System.currentTimeMillis());
             }
            }
        });

        buscar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                (MainActivity.this).adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                lista.getAdapter().getItem(i);
                Pessoa buscada = new Pessoa();
                for (Pessoa pessoa: pessoas) {
                    if(pessoa.getNome().equals(lista.getAdapter().getItem(i))){
                        buscada = pessoa;
                    }
                }
                cpf.setText(buscada.getCpf());
                nome.setText(buscada.getNome());
                SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
                try {
                    Date d = f.parse(buscada.getNascimento());
                    nascimento.setDate(d.getTime());

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                telefone.setText(buscada.getTelefone());
                celular.setText(buscada.getCelular());
                email.setText(buscada.getEmail());
            }
        });

        apaga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String removida = "";
                int count =0;
                for(String name: nomes){

                    if(name.equals(nome.getText().toString())){
                        removida = nomes.remove(count);
                    }
                    count++;
                }
                count = 0;
                for (Pessoa pessoa: pessoas) {

                    if(pessoa.getNome().equals(nome.getText().toString())){
                        pessoas.remove(count);
                    }
                    count++;
                }
                cpf.setText("");
                nome.setText("");
                celular.setText("");
                telefone.setText("");
                email.setText("");
                nascimento.setDate(System.currentTimeMillis());
                adapter.notifyDataSetChanged();
                lista.setAdapter(adapter);
                cpf.requestFocus();
                Toast.makeText(MainActivity.this, "Pessoa removida :"+removida, Toast.LENGTH_LONG).show();
            }
        });


    }
}